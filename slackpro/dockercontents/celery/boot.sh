#!/bin/bash
source venv/bin/activate

/venv/bin/celery -A app.celery worker --workdir=/slackpro
